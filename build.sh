#!/usr/bin/env bash

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "CMAKE_VARS=\"\" $0 <OS> <ARCH> <CONFIG> <TOOL>"
    exit 0
fi

# tolower arguments
G_OS="${1,,}"
G_ARCH="${2,,}"
G_CONFIG="${3,,}"
G_TOOL="${4,,}"

# ~~~~~~ parse args ~~~~~~
if [[ ! "${G_OS}" == "windows" ]]; then
    echo "${G_OS} is not a supported opperating system"
    exit 1
fi
if [[ ! "${G_ARCH}" == "x64" && ! "${G_ARCH}" == "x86" ]]; then
    echo "${G_ARCH} is not a supported architecture"
    exit 1
fi
if [[ ! "${G_CONFIG}" == "debug" && ! "${G_CONFIG}" == "release" ]]; then
    echo "${G_CONFIG} is not a supported configuration"
    exit 1
fi
if [[ ! "${G_TOOL}" == "default" && ! "${G_TOOL}" == "vc142" ]]; then
    echo "${G_TOOL} is not a supported toolchain"
    exit 1
fi

G_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE}")" && pwd)"
G_SOURCE_DIR="${G_SCRIPT_DIR}"
G_JOBS=`nproc`
G_CMAKE_VARS="-Wdev ${CMAKE_VARS}"

G_DEFAULT_BUILD_ROOT="${G_SCRIPT_DIR}build"
if [[ "${G_OS}" == "windows" ]]; then

    G_BUILD_ARGS="/maxcpucount:${G_JOBS} /consoleloggerparameters:ForceConsoleColor"

    if [[ "${G_TOOL}" == "vc142" || "${G_TOOL}" == "default" ]]; then
        G_TOOL="vc142"
        G_TOOLSET="-T v142"
        G_CMAKE_GENERATOR="Visual Studio 16 2019"
    else
        echo "${G_TOOL} is not supported on ${G_OS}"
        exit 1
    fi

    if [[ "${G_ARCH}" == "x64" ]]; then
        G_CMAKE_ARCH="-A ${G_ARCH}"
    elif [[ "${G_ARCH}" == "x86" ]]; then
        G_CMAKE_ARCH="-A Win32"
    fi

    G_BUILD_DIR="./${G_OS}/${G_TOOL}/${G_ARCH}/"
fi

if [[ "${G_CONFIG}" == "debug" ]]; then
    G_CMAKE_VARS="${G_CMAKE_VARS} -DDEBUG_SUFFIX='-debug'"
fi

mkdir -p "${G_BUILD_DIR}"
pushd "${G_BUILD_DIR}"
    cmake -G "${G_CMAKE_GENERATOR}" ${G_CMAKE_ARCH} ${G_TOOLSET} -DCPACK_GENERATOR=${G_CPACK_GENERATOR} ${G_CMAKE_VARS} "${G_SOURCE_DIR}"
    cmake --build . --config "${G_CONFIG}" -- ${G_BUILD_ARGS}
popd
